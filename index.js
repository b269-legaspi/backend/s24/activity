const getCube = (num) => console.log(`The cube of ${num} is ${num ** 3}.`);

getCube(2);

const address = ["258", "Washington Ave NW", "California", "90011"];
const [number, street, state, zipCode] = address;

console.log(`I live at ${number} ${street}, ${state} ${zipCode}`);

const animal = {
  animalName: "Lolong",
  species: "Saltwater crocodile",
  weight: "1075",
  feet: "20",
  inches: "3",
};

const { animalName, species, weight, feet, inches } = animal;

console.log(
  `${animalName} was a ${species}. He weighed ${weight} with a measurement of ${feet} ft ${inches} inches`
);

const numbersToLoop = [1, 2, 3, 4, 5, 100];
numbersToLoop.forEach((num) => console.log(num));

const reduceNumber = (arr) => arr.reduce((total, num) => total + num);
console.log(reduceNumber(numbersToLoop));

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const hotdog = new Dog("Hotdog", 5, "Dachshund");
console.log(hotdog);
